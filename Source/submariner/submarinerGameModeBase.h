// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "submarinerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SUBMARINER_API AsubmarinerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
