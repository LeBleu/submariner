// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SubmarinerPawnBase.generated.h"

UCLASS()
class SUBMARINER_API ASubmarinerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASubmarinerPawnBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	void ManageFasterKey();

	void ManageForwardAxis(float val);
	void ManageRightAxis(float val);
	void ManageUpAxis(float val);
	void ManageLookRight(float val);
	void ManageLookUp(float val);



private:
	float moveSpeed = 200.f; 
	FVector moveVector{ 0.f, 0.f, 0.f };
	FVector rotVector{ 0.f, 0.f, 0.f };


	void MoveFunction(float DeltaTime);
	void RotateFunction(float DeltaTime);
};
