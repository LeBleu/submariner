// Fill out your copyright notice in the Description page of Project Settings.


#include "SubmarinerPawnBase.h"

void ASubmarinerPawnBase::MoveFunction(float DeltaTime)
{
	if (!moveVector.Normalize())
		return;

	this->AddActorLocalOffset(moveVector * moveSpeed * DeltaTime);

	moveVector = FVector::ZeroVector;
}

void ASubmarinerPawnBase::RotateFunction(float Deltatime)
{
	if (!rotVector.IsNearlyZero())
	{
		FRotator wantedRot(rotVector.X, rotVector.Y, rotVector.Z);
		FRotator finalRot(GetActorRotation().Quaternion() * wantedRot.Quaternion());
		finalRot.Roll = 0.f;
		SetActorRotation(finalRot);
	}
	rotVector = FVector::ZeroVector;
}

// Sets default values
ASubmarinerPawnBase::ASubmarinerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASubmarinerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASubmarinerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MoveFunction(DeltaTime);
	RotateFunction(DeltaTime);
}

// Called to bind functionality to input
void ASubmarinerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("FasterKey", EInputEvent::IE_Pressed, this, &ASubmarinerPawnBase::ManageFasterKey);
	InputComponent->BindAction("FasterKey", EInputEvent::IE_Released, this, &ASubmarinerPawnBase::ManageFasterKey);

	InputComponent->BindAxis("ForwardAxis", this, &ASubmarinerPawnBase::ManageForwardAxis);
	InputComponent->BindAxis("RightAxis", this, &ASubmarinerPawnBase::ManageRightAxis);
	InputComponent->BindAxis("UpAxis", this, &ASubmarinerPawnBase::ManageUpAxis);
	InputComponent->BindAxis("LookRight", this, &ASubmarinerPawnBase::ManageLookRight);
	InputComponent->BindAxis("LookUp", this, &ASubmarinerPawnBase::ManageLookUp);
}

void ASubmarinerPawnBase::ManageFasterKey()
{

}

void ASubmarinerPawnBase::ManageForwardAxis(float val)
{
	moveVector.X = val;
}

void ASubmarinerPawnBase::ManageRightAxis(float val)
{
	moveVector.Y = val;
}

void ASubmarinerPawnBase::ManageUpAxis(float val)
{
	moveVector.Z = val;
}

void ASubmarinerPawnBase::ManageLookRight(float val)
{
	rotVector.Y = val;
}

void ASubmarinerPawnBase::ManageLookUp(float val)
{
	rotVector.X = val;
}