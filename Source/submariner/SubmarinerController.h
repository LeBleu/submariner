// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SubmarinerController.generated.h"

/**
 * 
 */
UCLASS()
class SUBMARINER_API ASubmarinerController : public APlayerController
{
	GENERATED_BODY()
	
};
